% SatNOGS COMMS System Design Document - Verification plan
% Copyright (C) 2020, 2022 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage{rotating}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Verification and Test Plan \\\large{SatNOGS COMMS}}
\author{Libre Space Foundation}
\date{\today\\Version \version\\\textit{DRAFT}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Introduction}

\section{Purpose and Scope}

This document describes verification and test procedures which verify that implementation of `SatNOGS-COMMS' complies with the defined System Requirement Specification.
It includes the approved test plan and procedures, test predictions, approved measurement point plan.

\printglossary[type=\acronymtype]

\chapter{Test Plan}

\section{Verification approach}

\subsection{Verification Methods}

Verification is accomplished with at least one of the methods bellow depending on schedule and cost constraints:
\begin{itemize}
\item Inspection:\\
  Verification by inspection consists of visual determination of physical characteristics, for example constructional features, hardware conformance to document drawing or workmanship requirements, physical conditions, software source code conformance with coding standards etc.
\item Review of design:\\
  Verification by \acrshort{rod} consists of using approved records or evidence that unambiguously show that the requirement is met.
\item Analysis:\\
  Verification by analysis consists of performing theoretical or empirical evaluation using techniques, such as systematic, statistical and
  qualitative design analysis, modeling and computational simulation etc.
\item Test:\\
  Verification by test consists of measuring product performance and functions under representative simulated environments.
\end{itemize}

\subsection{Verification Levels}

Requirements are verified in three different verification levels:

\begin{itemize}
\item System Level: The SatNOGS COMMS end-to-end system.
\item Segment level: The ground and space segments.
\item Equipment/Unit Level: The components of ground and space segments.
\end{itemize}

\section{Model philosophy}

The Model philosophy that is selected for SatNOGS COMMS is the hybrid model philosophy.
The objective of following this approach is the reduction of risks while maintaining a balance in schedule and cost.

The output of this activity is an \acrfull{eqm} for a new communication subsystem. By having a new design with \acrshort{cots} hardware and by following an agile development, the models needed for each verification level of SatNOGS COMMS are described bellow (\textit{Note: Some of the models, that will be used in different verification levels, may be identical/represented by the same hardware}):

\begin{itemize}
\item System Level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{stm}
  \item \acrfull{eqm}
  \item Suitcase Model
  \end{itemize}

\item Segment level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{eqm}
  \item Ground Segment Models
    \begin{itemize}
    \item Model for developing and verifying procedures and the \acrfull{hmi}
    \item Model for \acrshort{tcnc} data processing and associated procedures
    \end{itemize}
  \end{itemize}

\item Equipment/Unit Level
  \begin{itemize}
  \item \acrfull{mu}
  \item \acrfull{dm}

  \end{itemize}

\end{itemize}

\section{Verification strategy}

Verification strategy of SatNOGS COMMS is a combination of the different verification methods, giving emphasis on test method, at the different verification levels as these described on the previous section.
Following this strategy is the way to verify, test and fulfill requirements defined in System Requirement Specification and the corresponding GitLab requirement issues.

\section{Verification control methodology}

Monitoring and controlling the verification procedures is done through the issue tracker of open source git-repository manager GitLab by tracking verification issues for each method is applied at each level.
GitLab issue tracker offers organizing and managing tools in order to effectively monitor and control the verification process.

\chapter{Test Plan}

Verification by test is performed at all three verification levels of the SatNOGS COMMS system in order to ensure that System Requirement Specification is satisfied, test case and the requirements that cover can be found in Appendix A.

\section{Roles}

There are three roles that participate in the testing plan with respective responsibilities.

\begin{itemize}
\item Developer: \\
  Responsible for implementation of the technical specification and low-level test cases.
\item Tester: \\
  Responsible for implementing \acrshort{e2e} tests and testing the technical specification implementation.
\item Test Manager: \\
  Responsible for test plan execution by reviewing test reports and based on that accepting each level of technical specification implementation.
\end{itemize}

\section{Levels}

There are three different levels of testing:

\subsubsection{Unit Testing}

The purpose of this level is to test the smallest units of implementation and facilitate the development process.
Unit tests are implemented and executed by the developer.
There is no mapping or traceability of unit tests to system requirements.
Unit tests are executed automatically to generate test coverage reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Functional/Module Testing}

The purpose of this level is to verify correct implementation of low-level functional requirements for each of ground and space segment.
Functional tests are implemented by the developer and executed by the tester.
Mapping and traceability of functional tests to system requirements is provided indirectly through association of low-level functional requirements to high-level system requirements.
Functional tests are executed automatically and generate pass/fail reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Integration/End-to-end Testing}

The purpose of this level is to verify correct implementation of high-level system requirements.
\acrshort{e2e} tests are implemented and executed by the tester.
There are direct mapping and traceability of \acrshort{e2e} tests to system requirements.
\acrshort{e2e} tests are executed automatically and generate pass/fail reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\chapter{Tools  and test facilities}

In this section are described tools, \acrshort{gse} and test facilities that are available and required to execute the verification.

\subsubsection{Collection of test data}

From previous development efforts and SatNOGS development there are available random and structured data, frames etc.

\subsubsection{RF measurement equipment}

The electronics lab of \acrshort{lsf} facilities is equipped with Spectrum analyzers, power meters, attenuators, power supplies and vector network analyzers.
Additionally \acrshort{emc} testing facilities can be coordinated with EMC Hellas.

\subsubsection{UHF and S-band receivers and transmitters}

\acrshort{sdr} devices that meet required specifications are available in \acrshort{lsf} facilities.

\subsubsection{Software simulators/emulators}

\acrshort{cots} and open source simulators and emulators have been used in previous COMMS development efforts by \acrshort{lsf} and are available in \acrshort{lsf} facilities

\subsubsection{Microcontroller and FPGA equipment}

Programmers and development kits for microcontroller and FPGA exists in \acrshort{lsf} facilities.

\subsubsection{TVAC for environment testing}

\acrshort{lsf} has secured access to a Greek University TVAC facility that meets the verification and test requirements.

\subsubsection{Vibration testing equipment}

\acrshort{lsf} has secured access to the Hellenic Aerospace Industry Inc.\ vibrational testing facilities.

\begin{appendices}

  \chapter{Test Cases}

\end{appendices}

\end{document}
