cmake_minimum_required(VERSION 2.8)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/CMakeModules")

project(documentation NONE)

include(UseLATEX)

add_custom_command(
  OUTPUT
  include/requirements.tex
  COMMAND python3 ${CMAKE_SOURCE_DIR}/contrib/issue-template.py --project "librespacefoundation/satnogs-comms/satnogs-comms-org" --label "Requirement" --output-file include/requirements.tex "${CMAKE_SOURCE_DIR}/templates/include/requirements.tex.j2"
  )

add_latex_document(system-design.tex
  INPUTS
  glossaries/acronyms.tex
  include/requirements-urls.tex
  include/version.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS diagrams
  )
add_latex_document(design-development-plan.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  USE_GLOSSARY
  IMAGE_DIRS diagrams
  )
add_latex_document(quality-assurance-plan.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS diagrams
  )
add_latex_document(verification-plan.tex
  INPUTS
  glossaries/acronyms.tex
  include/requirements-urls.tex
  include/version.tex
  USE_GLOSSARY
  IMAGE_DIRS diagrams
  )
add_latex_document(test-readiness-report.tex
  INPUTS
  glossaries/acronyms.tex
  include/requirements-urls.tex
  include/replacements.tex
  include/version.tex
  USE_GLOSSARY
  )
add_latex_document(ICD.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS diagrams
  )
add_latex_document(system-requirements.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  DEPENDS
  include/requirements.tex
  USE_GLOSSARY
  IMAGE_DIRS diagrams
  )
